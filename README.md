# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
   prov - a project to do datacenter/application deployment, management in modern way

* Version
   0.0001 :)
### How do I get set up? ###

### Summary of set up
  You may install it in virtual env by following below steps

* install python-virtualenv

```
    $ apt-get install python-virtualenv 
    Reading package lists... Done
    Building dependency tree
```

* Run "virtualenv venv" on your workspace

```
    $ virtualenv venv
    New python executable in venv/bin/python
    Installing setuptools, pip...done.
```
    
* Activate your virtual env 

```

    $ source venv/bin/activate
    (venv)root@code1:~/test$
    
    (venv)root@code1:~/test# pip install -U pip
    Downloading/unpacking pip from https://pypi.python.org/packages/py2.py3/p/pip/pip-8.0.2-py2.py3-none-any.whl#md5=2056f553d5b593d3a970296f229c1b79
      Downloading pip-8.0.2-py2.py3-none-any.whl (1.2MB): 1.2MB downloaded
    Installing collected packages: pip
      Found existing installation: pip 1.5.4
        Uninstalling pip:
          Successfully uninstalled pip
    Successfully installed pip
    Cleaning up...
```

* Install prov using "pip install -e git+https://bitbucket.org/to3/prov.git#egg=prov"

```

    (venv)root@code1:~/test# pip install -e git+https://bitbucket.org/to3/prov.git#egg=prov
    Obtaining prov from git+https://bitbucket.org/to3/prov.git#egg=prov
      Cloning https://bitbucket.org/to3/prov.git to ./venv/src/prov
    (venv)root@code1:~/test#
```

* now on you may just activate the virtual env by following #3 and use prov

```

    (venv)root@code1:~/test# deactivate    # To get out of virtualenv
    
    root@code1:~/test# source venv/bin/activate	# To get into virtualenv
    (venv)root@code1:~/test# 

```

* Setup db (below instruction is to setup sqlite db)

```
(venv)root@code1:/vagrant/prov# python manage.py makemigrations
(venv)root@code1:/vagrant/prov# python manage.py migrate
```
* Start webserver

run django webserver using "python manage.py 0.0.0.0:8000". And start using it

```

(venv)root@code1:/vagrant/prov# python manage.py runserver 0.0.0.0:8000
Performing system checks...

System check identified no issues (0 silenced).
April 30, 2016 - 09:56:45
Django version 1.9.5, using settings 'project.settings'
Starting development server at http://0.0.0.0:8000/
Quit the server with CONTROL-C.
```

* Now you may point your browser to manage the resources as below

Manage providers, datacenters and stacks: http://localhost:8000/manager/

## Configuration
## Dependencies
## How to run tests
## Deployment instructions

### Contribution guidelines ###

##  Writing tests
## Code review
## Other guidelines

### Who do I talk to? ###

## Repo owner or admin
## Other community or team contact
