from rest_framework import serializers
from prov.django.apps.manager.models import Provider
from django.contrib.auth.models import User


class ProviderSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    url = serializers.HyperlinkedIdentityField(
        view_name='provider-detail',
        lookup_field='name'
    )

    class Meta:
        model = Provider
        fields = ('url', 'name', 'type', 'auth', 'owner')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    providers = serializers.HyperlinkedRelatedField(
        many=True, view_name='provider-detail', read_only=True, lookup_field='name')

    class Meta:
        model = User
        fields = ('url', 'username', 'providers')
