from prov.django.apps.manager.models import Provider
from prov.django.apps.manager.serializers import ProviderSerializer, UserSerializer
from django.contrib.auth.models import User
from rest_framework import permissions
from permissions import IsOwner

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from rest_framework.viewsets import ReadOnlyModelViewSet, ModelViewSet


@api_view(['GET'])
def api_root(request, format=None):
    return Response(
        {
            'users': reverse('user-list', request=request, format=format),
            'providers': reverse('provider-list', request=request, format=format)
        }
    )


class ProviderViewSet(ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,
                          IsOwner,)
    serializer_class = ProviderSerializer
    lookup_field = 'name'

    def get_queryset(self):
        return Provider.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class UserViewSet(ReadOnlyModelViewSet):
    permission_classes = (permissions.IsAdminUser,)
    queryset = User.objects.all()
    serializer_class = UserSerializer
