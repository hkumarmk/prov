Prov as next-gen distributed datacenter operating system
==================================================
* Manage resources remotely using cenral/single api/command/interface
* He can provision the nodes anywhere 
    * In my datacenter (using cobbler kind of systems, openstack ironic or anything else)
    * In public cloud - aws, google cloud, rackspace etc
    * In private cloud - openstack 
* He can deploy the services in different form in anywhere 
    * He may provision the nodes automatically per service or deploy the services on already provisioned nodes
    * as containers - docker, 
    * as separate vms - provision vms if not already there
    * Use existing machines to add services to it
        * May be u asked to use an existing machine
        * As per policy to jampack the nodes
        * The resource it supposed to use is configured by metadata - this will not enforce resource constraints
    * Each services will have 
        * dependencies,
        * verification steps,
        * monitoring required,
        * communications to dependencies, 
        * actions run on various communication from its providers and dependants
        * operations KB Database (for operations intelligence)
    * All services can validate themselves, register to autodiscovery, monitoring engine
    * Services can be connect/disconnect together, when this happen, 
        * each service will have series of communication
            * Service A ask service discovery for its dependency service X and will wait till the dependency available
            * once service X is available in service discovery, A will start communicating with X
                * A will say hello to X on which X may run some optional series of actions
                * A may send number of things it need from X, on which X will run appropriate actions, and respond to A
                * A may do number of actions once he got response from X after the above step
                * A and X may have number of followup communications if required in various events after that
                * All communications may be direct or indirect (may be through a central brocker - or using consul kv)
    * Service will install verification locally or remotely as required (need to see remote verification is required and if yes, how reliably can this happen
    * Service will register appropriate monitors in monitoring system
* He can manage service instance lifecycle centrally - 
    * Central service management system - to start/stop/restart/upgrade/rolling restart/rolling upgrade etc
* He can do orchestration
    * Service orchestration
    * upgrade orchestration
    * Any other operations which need orchestration
* He can be operations assistant
    * Operations intelligence
    * Analyze various datapoints from logs, monitors and other system stats and use them for operations and other activities
    * Do operations from central console/interface - client (thin/thick?), web, chat based operations interfaces
        * May be it Act as PIM?
            * all actions gets logged,
            * may be option to save sessions as videos?
            * Should be able to run predefined operations procedures (easy to use functions) and arbitrary commands
            * should be able to correlate user actions with system events
            * Should be able to manage users, groups, with role based access control (RBAC)
            * Should be able to restrict commands, systems to specific role/group
            * May be a way to share the session with others
            * asynchronous response when possible - web based interface should be able to provide 
            * Easy ways to run the commands/operations on group/individual servers
 * Resources can be provisioned individually like "prov deploy mysql -t dc1" or set/group of resources can be provisioned as template
 * systemwide version management system - somehow it should have a way to version and have history for all items
 * How to have a review system integrated, so all changes can be reviewed before commit

## Various engines which make prov system
 * Provisioning engine
 * Deployment engine
 * Monitoring Engine
 * Operations engine
 * Service discovery and orchestration engine
 * Client agent
 * Security engine
 
 May be
 
 * Testing engine 
 * cicd engine
 
 ## How it works

 * User define a datacenter
 * Datacenter may be explicitly initialized or automatically initialized when first workload (application or node)
 started in that datacenter
 * Datacenter may be synonymous to provider
 * Datacenter has properties
  * Datacenter may be logical or physical 
    * Logical: you can just group number of system to isolated group by creating a datacenter
    * Physical: may be separate systems in multiple regions/availabiliity zones of cloud or physical datacenter
* Datacenter will have a type - like aws, gce, physical dc-ironic, physicaldc - cobbler etc
* DC has endpoints or a code to provision servers (and other resources) in them
* DC might have other stuffs like separate keys etc
* Multiple DCs can be connected each other, if connected, services in those DCs, can see/access each other and do
    multi-dc orchestration
* Datacenter initialization
    * This can be done explicitely by user
    * During first workload deployment, in case the datacenter is not initialzed, it will automatically get initialized
    * The process
        * Ask provisioner to provision one or cluster of nodes on the datacenter which will have
        * Deploy orchestrator application, consul, monitoring, etc will be deployed on them
        * Get the datacenter ready to
            * service discovery and orchestration
            * core infra services like monitoring, log aggregation servers in place (optional, may be user choose to not
             to have monitoring system on individual dc)
            * deployer proxy/api server to deploy various apps without directly contacting individual servers
* All management (Provisioning, deployment, orchestration etc) can be done from any app servers in inter-connected
datacenters - all will get coordinated in case of requests to remote datacenter
* All management may also be able to do from a management workstation - prov as a command/api client
    * Individual DCs will have key/user credentials to access them, which need to be updated in the workstation
    * May be a yaml file which have all/multiple  datacenter configs from which prov client will read and execute the ops
    in appropriate datacenter
    
* User may just deploy an app on specific datacenter or one of the available datacenters
    * Applications may different properties
        * May be just run/create new app in existing application instance - like database, say if a mongo cluster is there, 
          may be adding a collection would just need to create it on existing mongodb cluster rather than creating a new
          cluster. mysql also have same property, if a new website, may be it is spawned in existing webservers.
            * may be it would need to define the overhead of the instance so that resources can be reserved per instance
            * In case of scaleout required in this case, it would automatically be happening - if mongodb cluster need to be
            extended, it will happen automatically by adding a mongodb shard, as required
            * In case of applicatoin doesnt support scaling out (multi-node cluster), like mysql, new instance may be required,
            new server instance created - say if database mydb need to be created and existing mysql server[s] are already
            overloaded, then a new server instance will be created and install the database in that server.
        * It may create containers on existing servers or if no free resources available, then boot a node and create container
        on that
        * It may boot a new node per application
            * Still there would be an option to create the application on existing node
    * Application or services may be connectable each other (see deployer doc)
    
* User may boot the nodes and deploy apps on it


        
            
   
