## Provider or datacenter (in context of Provisioner)
provider and datacenter is interchangable in context of provisioner, all I meant was to have a (isolated? may be)
group of resources managed in specific way. a provider/datacenter will have the following properties
    
* Type: type of datacenter which may be aws, openstack, etc
* Endpoint: provider may have an endpoint which contain api endpoints and auth credentials to it (may be we may make
    credentials as separate property, if it make sense)
* Access Method: Provider may either an end point or an access method (or class) using which provisioner can provision
    a supported resource on top of it.
* Name/id: to identify different providers

Note that provider/datacenter may have different meaning in context of deployer or some other modules which is something
will handle as and when it comes.