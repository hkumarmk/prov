## design pattern to make provisioner a state machine for datacenter/provider resources ##

First question - do we really need to keep the states and resource details within the provisioner? or isn't it good enough
to have them get directly from the provider as and when it get the query?

The reasons I thought of caching the resource state/info in provisioner is because of performance reasons, and/or may be
to have different maps of the data to have easily formated output/manipulation. 

The problem with caching is that, we need to make sure the data is consistent (so we will need to do sync in specific
frequency and may be on specific events occurs (may be on any specific requests ). Also we will load the resources from
the provider on startup.

## Glossary

[Read Glossary document](../Glossary.md) 

## Provisioner as state machine
Provisioner consists of classes for all objects - different providers/datacenters, different type of resources like
nodes, networks, etc.

Instance of different classes will hold methods as well as state of that particular one. For example, consider "Node" is
 a class for node, so instance of Node(name='node1') will have the methods to manage that node as well as the state/info
 of node1.
 
There will be resource registry class (e.g NodeRegistry for nodes) or something to keep track of all instance of that
specific resource. For example, NodeRegistry will have all instances of nodes within a datacenter/provider. There will be
__one and only one__ instance of NodeRegistry which will keep all instances of nodes.

All resources within a provider will be kept under one instance of that provider. So provider object will have
 * provider endpoint itself (a cloud object)
 * Any Properties of that particular provider
 * methods
 * All resource objects within that provider 
 
For example, consider "os_region1" is an openstack provider. it will contain objects of all resource registry objects
 within it (e.g instances of NodeRegistry, NetworkRegistry, VolumeRegistry, ImageRegistry etc which itself contain
 individual resource instances)
   
There will be __one and only one__ instance per provider which will have all Registry objects.
 
Now there will be a providerRegistry which will contain all providers registered within that provisioner, which contain
 all provider objects as mentioned above.
 
So here is a hierarchical (yaml) representation of the object hierarchy (below things represent different class objects):

```

provider_registry:
    os_region1:
        node_registry:
            [node1, node2, node3,...]
        network_registry:
            [net1, net2]
    os_region2:
    aws_singapore:
        node_registry:
            [vm1, vm2]
    digitalocean_singapore:  
            [do1, do2]
```

We might need to have different maps in order to have individual resources easily fetchable which may need to think
 as it comes.
 
We would use class variables may be along with weakref (I am not sure if we want to use weakref as weakref will allow
garbage collector to flush them if there is no other reference exist). Each registry classes will be responsible to 
gather all instances of the objects coming under them.

I am not sure if we need to use metaclass but I wouldnt be use it at this moment as I am not a python expert to use it.

## State persistance

Provisioner doesnt need to persist any resource state or resource config/info, unless he has anything in it. All state 
and resource details can be fetched/synced from the provider endpoint itself. So while starting provisioner, it will sync
all available resource details from the provider and keep it with him (so that he dont have to go back to the provider
for each query which would improve the performance)

## consistency

Provisioner will have to make sure the state/info consistency as it is possible to have difference on actual provider and
provisioner cached data. This need to be synced in specific duration as well as on certain events/requests happen.

## Reference

### Questions

http://stackoverflow.com/questions/4831307/is-it-bad-to-store-all-instances-of-a-class-in-a-class-field

http://stackoverflow.com/questions/12101958/keep-track-of-instances-in-python

http://stackoverflow.com/questions/6566767/how-to-obtain-all-instances-of-a-class-within-the-current-module

### Blog

http://www.toptal.com/python/python-class-attributes-an-overly-thorough-guide

### Sample from existing implimentations

contrail using class variable (dict) to store multiple objects

https://github.com/Juniper/contrail-controller/blob/ac0ac8a5680115cd9a4f038459c11f8e9c1a18a0/src/config/common/vnc_db.py#L12

https://github.com/Juniper/contrail-controller/blob/ac0ac8a5680115cd9a4f038459c11f8e9c1a18a0/src/config/common/vnc_db.py#L39-L72

https://github.com/Juniper/contrail-controller/blob/151ff9e5ae93d1523225fad1b9210f7be8f35bde/src/config/svc-monitor/svc_monitor/config_db.py#L21

https://github.com/Juniper/contrail-controller/blob/aba70e3da6b0ab61f35aa965d3ab4866d57e0492/src/config/schema-transformer/to_bgp.py#L409

Another one

https://github.com/aaSemble/python-aasemble.django/blob/master/aasemble/django/apps/buildsvc/pkgbuild/__init__.py#L264-L269

https://github.com/aaSemble/python-aasemble.django/blob/master/aasemble/django/apps/buildsvc/pkgbuild/golang.py#L18

### Metaclasses

http://www.onlamp.com/pub/a/python/2003/04/17/metaclasses.html

https://jakevdp.github.io/blog/2012/12/01/a-primer-on-python-metaclasses/