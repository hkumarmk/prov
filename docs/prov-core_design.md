* run as a daemon
* Stateless - all states will be written to data source
* horizontally scalable - because stateless
* Works as central manager for various modules
* handle stack data from data source
* handle state data from data source
* handle events from api server
  * regarding stack activation
  * on stack deactivation
  * on stack updation
* handle events from provisioner
  * on finishing the provisioner operations to send the ackednowledgement with
   status
  * A heartbeat rpc call which prov-core will decide to register or unregister
  the provisioner instance
  * may be an occational status report from provisioner may be through its
  capabilities or any other stuffs like the resources it is managing
* handle events from other components


Stack activation event
======================
  Api should have commited below things to the datasource before it send this
event to api-core.

1. stack or stackset data with a name or id to uniquely identify the stack
2. the provider - with a unique name/id, a provider type, endpoints and other
details

This event is generated to activate a named stack, on recieving
such event, core would be doing the following tasks:

1. get the stack data from the datasource
2. parse the stack data and extract granular actions
3. call provisioner with provider details which is required by provisioner.
   e.g provision one vm with certain details on provider dc-1
4. Accept the callback from provisioner on completing the provisioning
  4.1 in case of succussful provisioning, update the stack instance data, also
      update the dataset on the status
  4.2 In case of failure, do retry according to the stack configuration.
5. After completing all individual acctions, update datastore that stack
activation is completed



Questions
=========
* Does it make sense to avoid updating data source from api and make it through
apt-core? It may make sense in security perspective, so the user facing api
server will not have access to internal data source, it will always to through a
middleware which is prov-core.


Other stuffs
============

* Datacenter systems are location specific and may have different way of setup than applications
* Applications may or may not be location specific -
  * you may see applications which may run any systems in specific datacenter
  * Sometimes an applications may be setup across the datacenter (may be a DR setup) with some external load balancing system like dns based load balancing or something else
  * Sometimes specific applications may run on specific systems - hardware specific (i.e storage app should run on storage servers), or any type of servers (i.e all webservers need apache running)
  * Sometimes specific applications would need to run all systems in specific type (i.e a monitoring app should be running on ALL servers, a zmq client application should be running on all servers on which specific applications running on them which use zmq as messaging)


Details
=======

## prov-core (infrastructure stack as of now)

### ProvObject class

This is the base class for all stack objects. This should have/done below stuffs

* Set/Initialize all common data objects
* Set common methods to handle prov objects - those objects can be overridden from other classes when required.

#### ProvObject data
* data_input - input data required for that object and its child objects
* objects - a dict of all all child objects
* attrs - the object atributes
* description - object description
* object_type - type of object
* provider - provider on which the stack to be built - this should
* path - path of the object in the stack
* id - id of the object
* name - name of the object

#### ProvObject methods
* _process() - a method to process the stack items
* _provision() - a method to provision the stack objects

### StackManager class - class to manage stacks

  * This is a ProvObject with some methods overridden
  * Each stack will be identified with a name and uuid - if stack name is not unique, it will use uuid to identify the stack
  * Each stack will have a stack object associated with it which include complete data of a stack - stack object is a ProvObject
  * System will have single instance of this class which will have whole objects.
  * StackManager have some helper methods to add/list/delete/get info of stacks.
    * add(), remove(), list(), get(), export()

###
