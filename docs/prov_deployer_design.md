# Deployer

## Description
Deployer is responsible to deploy the apps using various methods on the machines. It support containers as deployment
mechanism and other deployment and configuration management systems like puppet, chef, ansible.
  
This will follow modular architecture for deployment mechanisms as well as application support. This will support 
base level applications like apache, mysql, tomcat etc or a high level application stack like openstack, openedx etc
which consist of number of base level applications.
  
Deployer use consul as a base mechanism for orchestration and provide a distributed data (configuration, system state
etc) store. Deployer expect consul cluster is up and agent is installed and configured and running (at least base 
configuration). Once deployer is up, it also manage consul configuration.

It may make sense to install a prov-agent which could be a wrapper on top of consul and would provide a capability to
handle full operations on the node.

The agent may be a container or may be application setup on the host itself.

## details

Model somewhat similar to juju

* Every "service" can be implemented with any mechanism - it can be bash, python, puppet, chef, ansible... anything
* Each service would have standard hooks - install, update, destroy, (annd may be others too)
* Each service would be supporting few more hooks - pre-connect, connect, post-connect, disconnect, post-disconnect etc
* say
  * App1 service will have install, update destroy and other hooks which will be called in appropriate events
  * App1 need mysql, so admin "connect" app1 to mysql, in which case
    * on connect:
      * app1 provide mysql following details 
	    * details required by mysql service to connect with app1 like db details to be created (db name, user credentials, any host level filter etc - there can be multiple app1 provide filters in which case it should create filter for both the hosts)
	    * Some details can be optional in which case mysql will create random values
	    * Optional actions to perform - may be mysql would be doing n actions on connect, but app1 only need mysql to do n-1 actions so it may say so
	  * App1 wait for mysql provide a "on connect message with any details and actions to be done onconnect - as mysql dont need app1 to do anything, he will just send a blank (or any such) message
	  * app1 just wait deployer to respond the details from mysql or just retry until it get details back
	  
	  * mysql was just waiting for app1's message for onconnect and once it get the message, parse it and collect actions (like create_db, create_user etc), and parameters (dbnames, users, grants etc)
      * mysql then run onconnect hook with actions and params as arguments to the hook script - it will create the db, users and grants as requested by app1
      * mysql then send connectack message with the details back to app1 which contain ack, and any required details - say if any params was random, like random password. it would be send back to app1, so it can use it - what information are required on ack should be provided by app1
    * postconnect
      * postconnect hook on app1 start and it may create tables and populate data on that,
      * postconnect hook on mysql is not required and thus would not be present and thus it will not be running on mysql
    * disconnect
      * app1 dont have any specific things to do, so no disconnect hook is present
      * mysql may take appropriate actions - disable users, hosts, archive the daabase and may be delete the database or any other actions.
      